package com.example.projectppsi.ui.home.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.ArticleListTempBody
import com.example.projectppsi.ui.detail_article.DetailArticleActivity
import kotlinx.android.synthetic.main.item_home_list_article.view.*

class ArticleListAdapter (private val article: List<ArticleListTempBody>): RecyclerView.Adapter<ArticleListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_home_list_article, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return article.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(article[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val articleTitle = view.txtArticleTitle
        private val articleDate = view.txtArticleDate
        private val articleUser = view.txtAdmin
        private val articleImage = view.imgArticle

        fun bindContact(article: ArticleListTempBody) {
            articleTitle.text = article.title
            articleDate.text = article.date
            articleUser.text = article.user

            Glide.with(this.itemView)
                .load(article.images[0])
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(articleImage)

            itemView.setOnClickListener {
                val i = Intent(itemView.context, DetailArticleActivity::class.java)
                itemView.context.startActivity(i)
            }

        }
    }
}