package com.example.projectppsi.ui.register

import com.example.projectppsi.model.response.RegisterSuccessResponse

interface RegisterContract {
    interface View {
        fun onLoading()
        fun onSuccess(response: RegisterSuccessResponse)
        fun onFailure()
    }

    interface Presenter {
        suspend fun sendRegisterData(username: String, email: String, password: String)
    }
}