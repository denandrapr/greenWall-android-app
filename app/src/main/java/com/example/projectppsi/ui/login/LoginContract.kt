package com.example.projectppsi.ui.login

import com.example.projectppsi.model.response.LoginSuccessResponse
import java.lang.Exception

interface LoginContract {
    interface View {
        fun onLoading()
        fun onSuccess(response: LoginSuccessResponse)
        fun onFailure(message: String)
    }

    interface Presenter {
        suspend fun sendLoginData(email: String, password: String)
    }
}