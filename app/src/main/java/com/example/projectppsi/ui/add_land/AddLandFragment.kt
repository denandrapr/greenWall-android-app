package com.example.projectppsi.ui.add_land

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.projectppsi.R
import com.example.projectppsi.ui.add_land_step_one.AddLandStepOneActivity
import kotlinx.android.synthetic.main.fragment_add_land.*

/**
 * A simple [Fragment] subclass.
 */
class AddLandFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_land, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addLand.setOnClickListener {
            val i = Intent(this.context, AddLandStepOneActivity::class.java)
            startActivity(i)
        }
    }

}
