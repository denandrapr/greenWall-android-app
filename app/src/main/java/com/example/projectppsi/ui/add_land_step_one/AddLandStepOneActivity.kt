package com.example.projectppsi.ui.add_land_step_one

import android.Manifest
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.example.projectppsi.R
import com.example.projectppsi.ui.add_land_step_two.AddLandStepTwoActivity
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_add_land_step_one.*
import kotlinx.android.synthetic.main.activity_chat_user.toolbar
import java.text.DateFormatSymbols
import java.util.*

class AddLandStepOneActivity : AppCompatActivity() {

    private var latitude : Double? = 0.0
    private var longitude : Double? = 0.0
    private lateinit var datePicked: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_land_step_one)

        toolbarSetup()
        buttonInit()
        getCurrentLocation()
        calendarSetup()
    }

    private fun getCurrentLocation() {
        val mFusedLocation = LocationServices.getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mFusedLocation.lastLocation.addOnSuccessListener(this
        ) { location ->
            latitude = location?.latitude
            longitude = location?.longitude
//            Log.d("My Current location", "Lat : ${location?.latitude} Long : ${location?.longitude}")
        }
    }

    private fun calendarSetup() {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        editTextDateEnd.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val months = DateFormatSymbols().months[monthOfYear]
                datePicked = "$dayOfMonth-$months-$year"
                editTextDateEnd.text = datePicked
            }, year, month, day)
            dpd.show()
        }
    }

    private fun buttonInit() {
        nextPage.setOnClickListener {
            val titleLand = editTextTitleLand.text.toString()
            val descLand = editTextDescLand.text.toString()

            val i = Intent(this, AddLandStepTwoActivity::class.java)
            i.putExtra("titleLand", titleLand)
            i.putExtra("descLand", descLand)
            i.putExtra("datePicked", datePicked)
            i.putExtra("latitude", latitude.toString())
            i.putExtra("longitude", longitude.toString())
            startActivity(i)
        }
    }

    private fun toolbarSetup() {
        this.setSupportActionBar(toolbar)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}