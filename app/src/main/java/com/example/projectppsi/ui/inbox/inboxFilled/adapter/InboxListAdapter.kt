package com.example.projectppsi.ui.inbox.inboxFilled.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.InboxListTempBody
import com.example.projectppsi.model.body.LandListTempBody
import com.example.projectppsi.ui.chat_user.ChatUserActivity
import com.example.projectppsi.ui.detail_land.DetailLandActivity
import kotlinx.android.synthetic.main.item_history_list.view.*
import kotlinx.android.synthetic.main.item_history_list.view.historyImage
import kotlinx.android.synthetic.main.item_home_list_land.view.*
import kotlinx.android.synthetic.main.item_home_list_land.view.landImage
import kotlinx.android.synthetic.main.item_home_list_land.view.txtLandLord
import kotlinx.android.synthetic.main.item_home_list_land.view.verifiedCheck
import kotlinx.android.synthetic.main.item_inbox_list.view.*

class InboxListAdapter (private val land: List<InboxListTempBody>): RecyclerView.Adapter<InboxListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_inbox_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return land.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(land[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val inboxName = view.txtInboxTitle
        private val inboxLastChat = view.txtInboxLastChat
        private val historyImage = view.inboxImage

        fun bindContact(inbox: InboxListTempBody) {
            inboxName.text = inbox.inboxName
            inboxLastChat.text = inbox.inboxLastChat

            Glide.with(this.itemView)
                .load(R.drawable.photo_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(historyImage)

            itemView.setOnClickListener {
                val i = Intent(itemView.context, ChatUserActivity::class.java)
                itemView.context.startActivity(i)
            }
        }
    }
}