package com.example.projectppsi.ui.profile_main

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

import com.example.projectppsi.R
import com.example.projectppsi.ui.activity.MainActivity
import com.example.projectppsi.ui.profile_edit.ProfileEditActivity
import com.example.projectppsi.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_profile_main.*
import kotlinx.android.synthetic.main.fragment_settings.*

class ProfileMainFragment : Fragment() {

    private lateinit var sharedRef : SharedPreferenceManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        buttonInit()

        Glide
            .with(view.context)
            .load(R.drawable.ic_placeholder_photo)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(photoProfile)
    }

    private fun buttonInit() {
        layoutLogOut.setOnClickListener {
            val builder = AlertDialog.Builder(this.requireContext())
            builder.setTitle("Logout")
            builder.setMessage("Apakah anda ingin logout?")
            builder.setIcon(R.drawable.ic_baseline_warning_24)

            builder.setPositiveButton("Yes"){_, _ ->
                sharedRef = SharedPreferenceManager.getInstance(requireContext())
                sharedRef.clearAllSharedPref()

                val i = Intent(context, MainActivity::class.java)
                startActivity(i)
                activity?.finish()
            }
            builder.setNegativeButton("No"){_, _ ->
            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }

        layoutEditProfile.setOnClickListener {
            val i = Intent(context, ProfileEditActivity::class.java)
            startActivity(i)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.setting -> {
                val navAction = ProfileMainFragmentDirections.destinationSetting()
                Navigation.findNavController(requireView()).navigate(navAction)
                true
            } else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.option_menu_for_profile, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}
