package com.example.projectppsi.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.example.projectppsi.R
import com.example.projectppsi.util.SharedPreferenceManager
import com.example.projectppsi.util.log
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setUpNavigation()

        log("${SharedPreferenceManager.getInstance(applicationContext).getToken}")
    }

    private fun setUpNavigation() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.navHostFragment) as NavHostFragment?
        NavigationUI.setupWithNavController(
            nav_bottom,
            navHostFragment!!.navController
        )
    }
}
