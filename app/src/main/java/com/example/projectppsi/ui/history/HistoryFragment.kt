package com.example.projectppsi.ui.history

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.projectppsi.R
import com.example.projectppsi.model.body.HistoryListTempBody
import com.example.projectppsi.model.body.LandListTempBody
import com.example.projectppsi.ui.history.adapter.HistoryListAdapter
import com.example.projectppsi.ui.home.adapter.LandListAdapter
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    private val landList = listOf(
        HistoryListTempBody("Butuh penghijauan di kos kami",  "Jl. Semampir Tengah 8A no 12", "Done"),
        HistoryListTempBody("Tanaman depan rumah kami tumbuhan nya mati", "Jl. Bratang Binangun VA", "On Progress"),
        HistoryListTempBody("Bantu kami menanam di kantor", "Jl. Semolowaru Tengah", "Pending"),
        HistoryListTempBody("Tidak ada tanaman di RT Kami", "Jl. Rungkut Harapan", "Done")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewHistory.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = HistoryListAdapter(landList)
        }
    }
}
