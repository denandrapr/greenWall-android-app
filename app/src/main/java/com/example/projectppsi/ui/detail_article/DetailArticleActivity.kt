package com.example.projectppsi.ui.detail_article

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.projectppsi.R
import com.synnapps.carouselview.ImageListener
import kotlinx.android.synthetic.main.activity_chat_user.toolbar


class DetailArticleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_article)

        toolbarSetup()
    }


    private fun toolbarSetup() {
        this.setSupportActionBar(toolbar)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
