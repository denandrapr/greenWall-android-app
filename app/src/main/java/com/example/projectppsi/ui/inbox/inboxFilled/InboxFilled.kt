package com.example.projectppsi.ui.inbox.inboxFilled

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projectppsi.R
import com.example.projectppsi.model.body.HistoryListTempBody
import com.example.projectppsi.model.body.InboxListTempBody
import com.example.projectppsi.ui.history.adapter.HistoryListAdapter
import com.example.projectppsi.ui.inbox.inboxFilled.adapter.InboxListAdapter
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_inbox_filled.*

class InboxFilled : Fragment() {

    private val inboxList = listOf(
        InboxListTempBody("Dimas",  "OTW gan"),
        InboxListTempBody("Bastian", "Oke siapp..."),
        InboxListTempBody("Rofi", "Yaapa bro")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inbox_filled, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerListInbox.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = InboxListAdapter(inboxList)
        }
    }

}