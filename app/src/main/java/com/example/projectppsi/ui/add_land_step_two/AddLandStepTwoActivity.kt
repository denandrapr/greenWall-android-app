package com.example.projectppsi.ui.add_land_step_two


import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.add_land.Description
import com.example.projectppsi.model.response.add_land.AddLandResponse
import com.example.projectppsi.util.SharedPreferenceManager
import com.example.projectppsi.util.log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.kroegerama.imgpicker.BottomSheetImagePicker
import kotlinx.android.synthetic.main.activity_add_land_step_two.*
import kotlinx.android.synthetic.main.activity_chat_user.toolbar


class AddLandStepTwoActivity : AppCompatActivity(),
    AddLandStepTwoContract.View,
    BottomSheetImagePicker.OnImagesSelectedListener {

    private lateinit var addLandStepTwoPresenter: AddLandStepTwoPresenter
    private lateinit var mapFragment : SupportMapFragment
    private lateinit var googleMap: GoogleMap
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private lateinit var datePicked: String
    private lateinit var titleLand: String
    private lateinit var descLand: String
    private var mStoreRef = FirebaseStorage.getInstance().reference
    private lateinit var uri: List<Uri>
    private val listUrl = mutableListOf<String>()
    private lateinit var downloadUri: Uri
    private var uriDownloaded = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_land_step_two)

        getIntentData()
        toolbarSetup()
        buttonInit()
        mapInit()

        addLandStepTwoPresenter = AddLandStepTwoPresenter(this)
    }

    private fun getIntentData() {
        latitude = intent.getStringExtra("latitude")!!.toDouble()
        longitude = intent.getStringExtra("longitude")!!.toDouble()
        datePicked = intent.getStringExtra("datePicked")!!
        titleLand = intent.getStringExtra("titleLand")!!
        descLand = intent.getStringExtra("descLand")!!
    }

    private fun buttonInit() {

        uploadImage.setOnClickListener {
            BottomSheetImagePicker.Builder("com.example.projectppsi.ui.fileprovider")
                .multiSelect(2, 4)
                .requestTag("multi")
                .show(supportFragmentManager)
        }

        btnFinish.setOnClickListener {
            uploadImageToFirebase()

            val totalVolunteer = editTextTotalVolunteer.text.toString()
            val areaSize = editTextAreaSize.text.toString()
            val locationLand = editTextTitleLocation.text.toString()
            val city = editTextCity.text.toString()
            val province = editTextProvince.text.toString()
            val postalCode = editTextPostalCode.text.toString()

            val descBody = Description(
                areaSize,
                datePicked,
                descLand
            )

//            val body = AddLandBody(
//                descBody,
//
//            )
        }
    }

    private fun uploadImageToFirebase() {

        for ((index, i) in uri.withIndex()) {
            val ref: StorageReference = mStoreRef.child("fotoLahan/${System.currentTimeMillis()}")
            val uploadTask = ref.putFile(uri[index])
            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                ref.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    downloadUri = task.result!!
                    uriDownloaded = task.result!!.toString()
                    listUrl.add(uriDownloaded)
                }
            }.addOnFailureListener {

            }
        }

        Log.d("Response download URL ", "$listUrl")
    }

    private fun mapInit() {

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            googleMap = it

            val location = LatLng(latitude, longitude)
            googleMap.addMarker(MarkerOptions().position(location).title("Lokasi lahan"))
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15f))
        }
    }

    private fun toolbarSetup() {

        this.setSupportActionBar(toolbar)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onImagesSelected(uris: List<Uri>, tag: String?) {

        uri = uris

        Glide
            .with(this)
            .load(uris[0])
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(imgUpload1)

        Glide
            .with(this)
            .load(uris[1])
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(imgUpload2)

        Glide
            .with(this)
            .load(uris[2])
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(imgUpload3)
    }

    override fun onLoading() {

    }

    override fun onSuccess(response: AddLandResponse) {

    }

    override fun onFailure(message: String) {

    }
}