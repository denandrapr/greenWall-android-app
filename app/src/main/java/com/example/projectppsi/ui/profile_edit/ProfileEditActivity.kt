package com.example.projectppsi.ui.profile_edit

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.util.log
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.activity_chat_user.toolbar
import kotlinx.android.synthetic.main.activity_profile_edit.*
import java.util.*

class ProfileEditActivity : AppCompatActivity() {

    private val IMAGE_CAPTURE_KTP_CODE = 1000;
    private val PERMISSION_KTP_CODE = 1001;
    private val IMAGE_CAPTURE_SELFIE_KTP_CODE = 1000;
    private val PERMISSION_SELFIE_KTP_CODE = 1001;
    var imageUriKtp: Uri? = null
    var imageUriSelfie: Uri? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)

        imageInit()
        toolbarSetup()
        calendarSetup()
        imagePicker()
    }

    private fun imagePicker() {
        buttonProfileImagePicker.setOnClickListener {
            ImagePicker.with(this)
                .crop()
                .compress(1024)
                .start()
        }

        uploadKtpPhoto.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    requestPermissions(permissions, PERMISSION_KTP_CODE);
                }
                else{
                    openCamera();
                }
            }
            else{
                openCamera();
            }
        }

        uploadKtpSelfiePhoto.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    requestPermissions(permissions, PERMISSION_KTP_CODE);
                }
                else{
                    openCameraSelfie();
                }
            }
            else{
                openCameraSelfie();
            }
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        imageUriKtp = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriKtp)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_KTP_CODE)
    }

    private fun openCameraSelfie() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        imageUriSelfie = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriSelfie)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_SELFIE_KTP_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            PERMISSION_KTP_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    openCamera()
                }
                else{
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
            PERMISSION_SELFIE_KTP_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    openCamera()
                }
                else{
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
            else -> {

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_KTP_CODE) {
            Glide.with(this)
                .load(imageUriKtp)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .fitCenter()
                .into(imgPlaceholderKtp)
        } else if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_SELFIE_KTP_CODE) {
            Glide.with(this)
                .load(imageUriSelfie)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .fitCenter()
                .into(imgPlaceholderSelfie)
        } else {
            if (resultCode == Activity.RESULT_OK) {
                val fileUri = data?.data
                Glide
                    .with(this)
                    .load(fileUri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .circleCrop()
                    .into(editPhotoProfile)
            }
        }
    }

    private fun calendarSetup() {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        buttonDateBirth.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in TextView
                val datePicked = "$dayOfMonth/${monthOfYear+1}/$year"
                buttonDateBirth.text = datePicked
            }, year, month, day)
            dpd.show()
        }
    }

    private fun imageInit() {
        Glide
            .with(this)
            .load(R.drawable.ic_placeholder_photo)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(editPhotoProfile)
    }

    private fun toolbarSetup() {
        this.setSupportActionBar(toolbar)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}