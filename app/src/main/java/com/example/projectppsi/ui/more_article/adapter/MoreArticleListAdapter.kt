package com.example.projectppsi.ui.more_article.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.ArticleListTempBody
import kotlinx.android.synthetic.main.item_more_article_article.view.*

class MoreArticleListAdapter (private val article: List<ArticleListTempBody>): RecyclerView.Adapter<MoreArticleListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_more_article_article, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return article.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(article[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val articleTitle = view.txtArticleTitle
        private val articleDate = view.txtArticleDate
        private val articleUser = view.txtAdmin
        private val articleImage = view.articleImage

        fun bindContact(article: ArticleListTempBody) {
            articleTitle.text = article.title
            articleDate.text = article.date
            articleUser.text = article.user

            Glide.with(this.itemView)
                .load(R.drawable.bg_green)
                .into(articleImage)
        }
    }
}