package com.example.projectppsi.ui.more_lands

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.projectppsi.R
import com.example.projectppsi.model.body.LandListTempBody
import com.example.projectppsi.ui.more_lands.adapter.MoreLandListAdapter
import kotlinx.android.synthetic.main.fragment_more_lands.*

/**
 * A simple [Fragment] subclass.
 */
class MoreLandsFragment : Fragment() {

    private val landList = listOf(
        LandListTempBody("Butuh penghijauan di kos kami", "Diarto", "Jl. Semampir Tengah 8A no 12","32"),
        LandListTempBody("Tanaman depan rumah kami tumbuhan nya mati","Joko Susilo", "Jl. Bratang Binangun VA","12"),
        LandListTempBody("Bantu kami menanam di kantor","Bobi", "Jl. Semolowaru Tengah","22"),
        LandListTempBody("Tidak ada tanaman di RT Kami","Mahmud", "Jl. Rungkut Harapan","16"),
        LandListTempBody("Bantu penanaman di panti asuhan kami","Billy B", "Jl. Arief Rahman Hakim","5"),
        LandListTempBody("Tolong bantu menanam di kampus kami","Bastian", "Jl. Kedung Baruk no 98","5")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_more_lands, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)

        recyclerMoreLands.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = MoreLandListAdapter(landList)
        }
    }

}
