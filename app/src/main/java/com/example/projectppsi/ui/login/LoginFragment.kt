package com.example.projectppsi.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.projectppsi.R
import com.example.projectppsi.model.response.LoginSuccessResponse
import com.example.projectppsi.ui.activity.HomeActivity
import com.example.projectppsi.util.Coroutines
import com.example.projectppsi.util.SharedPreferenceManager
import com.example.projectppsi.util.log
import com.example.projectppsi.util.toast
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.lang.Exception

class LoginFragment : Fragment(), LoginContract.View {

    private lateinit var loginPresenter: LoginPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginPresenter = LoginPresenter(this)
        buttonAction()
    }

    private fun buttonAction() {

        buttonLogin.setOnClickListener {
            val email = textEmail.text.toString()
            val password = textPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                context?.toast("Kolom harus diisi")
            } else {
                if (password.length < 6) {
                    context?.toast("password tidak boleh kurang dari 6 karakter")
                } else {
                    Coroutines.main {
                        loginPresenter.sendLoginData(email, password)
                    }
                }
            }
        }
    }

    override fun onLoading() {
        constraintLayout.visibility = View.GONE
        spin_kit.visibility = View.VISIBLE
        textLoading.visibility = View.VISIBLE
    }

    override fun onSuccess(response: LoginSuccessResponse) {
        SharedPreferenceManager.getInstance(requireContext().applicationContext).saveToken(response.token)

        val i = Intent(context, HomeActivity::class.java)
        startActivity(i)
        activity?.finish()
    }

    override fun onFailure(message: String) {
        constraintLayout.visibility = View.VISIBLE
        spin_kit.visibility = View.GONE
        textLoading.visibility = View.GONE
        Toast.makeText(this.requireContext(), message, Toast.LENGTH_SHORT).show()
    }
}
