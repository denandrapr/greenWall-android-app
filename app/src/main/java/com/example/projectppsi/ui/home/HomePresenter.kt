package com.example.projectppsi.ui.home

import com.example.projectppsi.networking.NetworkFactory
import com.example.projectppsi.ui.login.LoginContract

class HomePresenter(
    private val view: HomeContract.View
): HomeContract.Presenter {

    override suspend fun getDataArticle() {

        val service = NetworkFactory.serviceNoAuth()
        val request = service.getArticleLimitedFive()

        if (request.isSuccessful) {
            val response = request.body()
            if (response!!.success) {
                view.onSuccessGetArticle(response)
            } else {
                view.onFailure(response.messages)
            }
        } else {
            view.onFailure("Gagal mengambil data!")
        }
    }
}