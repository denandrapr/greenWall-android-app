package com.example.projectppsi.ui.register

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.projectppsi.R
import com.example.projectppsi.model.response.RegisterSuccessResponse
import com.example.projectppsi.ui.activity.HomeActivity
import com.example.projectppsi.util.Coroutines
import com.example.projectppsi.util.SharedPreferenceManager
import com.example.projectppsi.util.toast
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.android.ext.android.inject

class RegisterFragment : Fragment(), RegisterContract.View{

    private lateinit var presenter: RegisterPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = RegisterPresenter(this)
        buttonAction()
    }

    private fun buttonAction() {

        buttonRegister.setOnClickListener {
            val username = textUsername.text.toString()
            val email = textEmail.text.toString()
            val password = textPassword.text.toString()
            val rePassword = textRePassword.text.toString()

            if (username.isEmpty() || email.isEmpty() || password.isEmpty() || rePassword.isEmpty()) {
                context?.toast("Kolom harus diisi")
            } else if (password.length < 6 || rePassword.length < 6) {
                context?.toast("password tidak boleh kurang dari 6 karakter")
            } else if (password != rePassword) {
                context?.toast("password tidak sama")
            } else {
                Coroutines.main {
                    presenter.sendRegisterData(username, email, password)
                }
            }
        }
    }

    override fun onLoading() {
        constraintLayout.visibility = View.GONE
        spin_kit.visibility = View.VISIBLE
        textLoading.visibility = View.VISIBLE
    }

    override fun onSuccess(response: RegisterSuccessResponse) {
        SharedPreferenceManager.getInstance(requireContext().applicationContext).saveToken(response.token)

        val i = Intent(context, HomeActivity::class.java)
        startActivity(i)
        activity?.finish()
    }

    override fun onFailure() {

    }
}
