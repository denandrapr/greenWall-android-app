package com.example.projectppsi.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.ArticleListTempBody
import com.example.projectppsi.model.body.LandListTempBody
import com.example.projectppsi.model.response.article_limited_five.ArticleLimitedFiveResponse
import com.example.projectppsi.ui.home.adapter.ArticleListAdapter
import com.example.projectppsi.ui.home.adapter.LandListAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment(), HomeContract.View {

    private lateinit var homePresenter: HomePresenter
    private val listArticle = mutableListOf<ArticleListTempBody>()

    private val landList = listOf(
        LandListTempBody("Butuh penghijauan di kos kami", "Diarto", "Jl. Semampir Tengah 8A no 12","12"),
        LandListTempBody("Tanaman depan rumah kami tumbuhan nya mati","Joko Susilo", "Jl. Bratang Binangun VA","12"),
        LandListTempBody("Bantu kami menanam di kantor","Bobi", "Jl. Semolowaru Tengah","12"),
        LandListTempBody("Tidak ada tanaman di RT Kami","Mahmud", "Jl. Rungkut Harapan","12")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homePresenter = HomePresenter(this)

        recyclerListLand.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = LandListAdapter(landList)
        }

        Glide.with(view.context)
            .load(R.drawable.ic_for_home_banner)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .override(350)
            .into(icBanner)

        buttonAction()

        GlobalScope.launch(Dispatchers.IO) {
            homePresenter.getDataArticle()
        }
    }

    private fun buttonAction() {
        readMoreForLand.setOnClickListener {
            val navAction = HomeFragmentDirections.actionHomeFragmentToMoreLandsFragment()
            Navigation.findNavController(it).navigate(navAction)
        }

        readMoreForArticle.setOnClickListener {
            val navAction = HomeFragmentDirections.actionHomeFragmentToMoreArticleFragment2()
            Navigation.findNavController(it).navigate(navAction)
        }
    }

    override fun onLoading() {

    }

    @SuppressLint("SimpleDateFormat")
    override fun onSuccessGetArticle(response: ArticleLimitedFiveResponse) {
        for(i in response.article.indices) {

            val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val output = SimpleDateFormat("dd MMMM yy")

            var date: Date? = null
            try {
                date = input.parse(response.article[i].createdAt)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val formattedDate: String = output.format(date)

            listArticle.add(
                ArticleListTempBody(
                    response.article[i].title,
                    formattedDate,
                    "Admin",
                    response.article[i].image)
            )
        }

        if (listArticle.size > 0) {
            GlobalScope.launch (Dispatchers.Main) {
                recyclerListArticle.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                    adapter = ArticleListAdapter(listArticle)
                }
            }
        }
    }

    override fun onFailure(message: String) {

    }
}
