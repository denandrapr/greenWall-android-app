package com.example.projectppsi.ui.login

import com.example.projectppsi.model.body.LoginBody
import com.example.projectppsi.networking.NetworkFactory
import java.lang.Exception

class LoginPresenter(
    private val view: LoginContract.View
): LoginContract.Presenter {

    override suspend fun sendLoginData(email: String, password: String) {

        view.onLoading()

        val services = NetworkFactory.serviceNoAuth()
        val request = services.userLogin(email, password)

        if (request.isSuccessful) {
            val response = request.body()
            if (response!!.success) {
                view.onSuccess(response)
            } else {
                view.onFailure(response.messages)
            }
        } else {
            view.onFailure("Email tidak terdaftar")
        }
    }
}