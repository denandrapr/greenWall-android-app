package com.example.projectppsi.ui.add_land_step_two

import android.content.Context
import com.example.projectppsi.model.body.add_land.AddLandBody
import com.example.projectppsi.model.response.add_land.AddLandResponse

interface AddLandStepTwoContract {
    interface View {
        fun onLoading()
        fun onSuccess(response: AddLandResponse)
        fun onFailure(message: String)
    }

    interface Presenter {
        suspend fun sendLandData(context: Context, body: AddLandBody)
    }
}