package com.example.projectppsi.ui.add_land_step_two

import android.content.Context
import com.example.projectppsi.model.body.add_land.AddLandBody
import com.example.projectppsi.networking.NetworkFactory

class AddLandStepTwoPresenter(
    private val view: AddLandStepTwoContract.View
): AddLandStepTwoContract.Presenter {

    override suspend fun sendLandData(context: Context, body: AddLandBody) {

        view.onLoading()

        val services = NetworkFactory.serviceWithAuth(context)
        val request = services.addLand(body)

        if (request.isSuccessful) {
            val response = request.body()
            if (response!!.success) {
                view.onSuccess(response)
            } else {
                view.onFailure(response.messages)
            }
        } else {
            view.onFailure("Email tidak terdaftar")
        }
    }
}