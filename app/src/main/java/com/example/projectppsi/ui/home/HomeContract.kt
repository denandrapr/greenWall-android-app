package com.example.projectppsi.ui.home

import com.example.projectppsi.model.response.article_limited_five.ArticleLimitedFiveResponse

interface HomeContract {
    interface View {
        fun onLoading()
        fun onSuccessGetArticle(response: ArticleLimitedFiveResponse)
        fun onFailure(message: String)
    }

    interface Presenter {
        suspend fun getDataArticle()
    }
}