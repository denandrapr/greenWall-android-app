package com.example.projectppsi.ui.welcome_screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.projectppsi.R
import kotlinx.android.synthetic.main.fragment_welcome_screen.*

class WelcomeScreenFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonInit()
    }

    private fun buttonInit() {

        btnSignUp.setOnClickListener {
            val navAction =
                WelcomeScreenFragmentDirections.directionRegister()
            Navigation.findNavController(it).navigate(navAction)
        }

        textSignIn.setOnClickListener {
            val navAction =
                WelcomeScreenFragmentDirections.directionLogin()
            Navigation.findNavController(it).navigate(navAction)
        }
    }
}
