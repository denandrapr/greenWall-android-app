package com.example.projectppsi.ui.home.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.LandListTempBody
import com.example.projectppsi.ui.detail_article.DetailArticleActivity
import com.example.projectppsi.ui.detail_land.DetailLandActivity
import kotlinx.android.synthetic.main.item_home_list_land.view.*

class LandListAdapter (private val land: List<LandListTempBody>): RecyclerView.Adapter<LandListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_home_list_land, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return land.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(land[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val landTitle = view.txtLandTitle
        private val landLord = view.txtLandLord
        private val landLocation = view.txtLandLocation
        private val daysRemaining = view.txtDaysRemaining
        private val verifiedCheck = view.verifiedCheck
        private val imageLand = view.landImage

        fun bindContact(land: LandListTempBody) {
            landTitle.text = land.landTitle
            landLord.text = land.landLord
            landLocation.text = land.landLocation
            daysRemaining.text = land.daysRemaining

            Glide.with(this.itemView)
                .load(R.drawable.unnamed)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageLand)

            Glide
                .with(this.itemView)
                .load(R.drawable.ic_check_box_black_24dp)
                .override(40)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(verifiedCheck)

            itemView.setOnClickListener {
                val i = Intent(itemView.context, DetailLandActivity::class.java)
                itemView.context.startActivity(i)
            }
        }
    }
}