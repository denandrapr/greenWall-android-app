package com.example.projectppsi.ui.history.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.model.body.HistoryListTempBody
import com.example.projectppsi.model.body.LandListTempBody
import kotlinx.android.synthetic.main.item_history_list.view.*
import kotlinx.android.synthetic.main.item_home_list_land.view.*
import kotlinx.android.synthetic.main.item_home_list_land.view.landImage
import kotlinx.android.synthetic.main.item_home_list_land.view.txtLandLord
import kotlinx.android.synthetic.main.item_home_list_land.view.verifiedCheck

class HistoryListAdapter (private val land: List<HistoryListTempBody>): RecyclerView.Adapter<HistoryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_history_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return land.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(land[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val historyTitle = view.txtHistoryTitle
        private val historyLocation = view.txtHistoryLocation
        private val historyProgress = view.txtHistoryProgress
        private val historyImage = view.historyImage
        private val historyVerified = view.verifiedCheck

        fun bindContact(history: HistoryListTempBody) {
            historyTitle.text = history.historyTitle
            historyLocation.text = history.historyLocation
            historyProgress.text = history.historyProgress

            Glide.with(this.itemView)
                .load(R.drawable.unnamed)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(historyImage)

            Glide
                .with(this.itemView)
                .load(R.drawable.ic_check_box_black_24dp)
                .override(40)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(historyVerified)
        }
    }
}