package com.example.projectppsi.ui.register

import com.example.projectppsi.networking.NetworkFactory

class RegisterPresenter(
    private val view: RegisterContract.View
) : RegisterContract.Presenter {

    override suspend fun sendRegisterData(username: String, email: String, password: String) {
        view.onLoading()

        val services = NetworkFactory.serviceNoAuth()
        val request = services.userRegister(username, email, password)

        if (request.isSuccessful) {
            val response = request.body()
            view.onSuccess(response!!)
        } else {
            view.onFailure()
        }
    }
}