package com.example.projectppsi.ui.setting_page

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.projectppsi.R
import com.example.projectppsi.ui.activity.MainActivity
import com.example.projectppsi.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingFragment : Fragment() {

    private lateinit var sharedRef : SharedPreferenceManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonAction()
    }

    private fun buttonAction() {
        card_logout.setOnClickListener {
            sharedRef = SharedPreferenceManager.getInstance(requireContext())
            sharedRef.clearAllSharedPref()

            val i = Intent(context, MainActivity::class.java)
            startActivity(i)
            activity?.finish()
        }
    }
}
