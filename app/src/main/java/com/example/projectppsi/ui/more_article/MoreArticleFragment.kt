package com.example.projectppsi.ui.more_article

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.projectppsi.R
import com.example.projectppsi.model.body.ArticleListTempBody
import com.example.projectppsi.ui.more_article.adapter.MoreArticleListAdapter
import kotlinx.android.synthetic.main.fragment_more_article.*

/**
 * A simple [Fragment] subclass.
 */
class MoreArticleFragment : Fragment() {

//    private val articleList = listOf(
//        ArticleListTempBody("Bagaimana cara menanam mawar dirumah", "10 Mei 2020", "Admin"),
//        ArticleListTempBody("Apasaja bunga bunga yang tidak boleh ditanam di rumah","7 Mei 2020", "Admin"),
//        ArticleListTempBody("Cara menyiapkan tanah untuk menanam pisang","6 Mei 2020", "Admin")
//    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more_article, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)

//        recyclerMoreArticle.apply {
//            layoutManager = LinearLayoutManager(context)
//            adapter = MoreArticleListAdapter(articleList)
//        }
    }

}
