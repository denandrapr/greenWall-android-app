package com.example.projectppsi.ui.detail_land

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.projectppsi.R
import com.example.projectppsi.ui.chat_user.ChatUserActivity
import com.example.projectppsi.ui.join_volunteer.JoinVolunteerActivity
import com.example.projectppsi.util.toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.synnapps.carouselview.ImageListener
import kotlinx.android.synthetic.main.activity_chat_user.*
import kotlinx.android.synthetic.main.activity_detail_land.*
import kotlinx.android.synthetic.main.activity_detail_land.photoProfile
import kotlinx.android.synthetic.main.activity_detail_land.toolbar
import kotlinx.android.synthetic.main.bottom_sheet_for_join_land.view.*
import kotlinx.android.synthetic.main.bottom_sheet_profile.view.*
import kotlinx.android.synthetic.main.fragment_profile_main.*

class DetailLandActivity : AppCompatActivity() {

    lateinit var mapFragment : SupportMapFragment
    lateinit var googleMap: GoogleMap

    private var sampleImages = intArrayOf(
        R.drawable.sample1,
        R.drawable.sample2,
        R.drawable.unnamed
    )

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_land)

        toolbarSetup()
        buttonInit()
        mapInit()
        bottomSheetJoinSetup()
        bottomSheetProfileSetup()

        carouselView.pageCount = sampleImages.size
        carouselView.setImageListener(imageListener)

        Glide
            .with(this)
            .load(R.drawable.photo_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(photoProfile)
    }

    private fun mapInit() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback {
            googleMap = it

            val location = LatLng(-7.3079217, 112.7796523)
            googleMap.addMarker(MarkerOptions().position(location).title("Lokasi lahan"))
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15f))
        })
    }

    private fun bottomSheetProfileSetup() {
        val bottomSheetDialog = BottomSheetDialog(this)

        val view = layoutInflater.inflate(R.layout.bottom_sheet_profile, null)
        bottomSheetDialog.setContentView(view)

        Glide
            .with(this)
            .load(R.drawable.photo_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .circleCrop()
            .into(view.photoProfile)

        relativeProfile.setOnClickListener {
            bottomSheetDialog.show()
        }
    }

    private fun bottomSheetJoinSetup() {
        val bottomSheetDialog = BottomSheetDialog(this)

        val view = layoutInflater.inflate(R.layout.bottom_sheet_for_join_land, null)
        bottomSheetDialog.setContentView(view)

        joinVolunteer.setOnClickListener {
            bottomSheetDialog.show()
        }

        view.joinVolunteer.setOnClickListener {
            if (view.checkBoxOne.isChecked && view.checkBoxTwo.isChecked) {
                val i = Intent(this, JoinVolunteerActivity::class.java)
                startActivity(i)
            } else {
                this.toast("ketentuan belum di setujui")
            }
        }
    }

    private fun buttonInit() {

        btnSendMessage.setOnClickListener {
            val i = Intent(this, ChatUserActivity::class.java)
            startActivity(i)
        }
    }

    private var imageListener =
        ImageListener { position, imageView -> imageView.setImageResource(sampleImages[position]) }

    private fun toolbarSetup() {
        this.setSupportActionBar(toolbar)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar?.setDisplayShowTitleEnabled(false)
        this.supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}