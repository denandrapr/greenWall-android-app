package com.example.projectppsi.ui.chat_user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.projectppsi.R
import com.example.projectppsi.networking.SocketsChatApplication
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_more_article.*

class ChatUserActivity : AppCompatActivity() {

    private var mSocket: Socket? = null
    private var isConnected = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_user)

        toolbarInit()
        socketInit()
    }

    private fun socketInit() {
        val app = SocketsChatApplication()
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.connect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread() {
            Log.d("sockets", "${it.toList()}")
            if (!isConnected) {
                try {
                    mSocket!!.emit("userJoined", "Bob")
                    isConnected = true
                } catch (e: Exception) {
                    Log.d("Connected", "false $e")
                }
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
            Log.i("Connected", "diconnected")
            isConnected = false
            Toast.makeText(
                this.applicationContext,
                "Disconnected", Toast.LENGTH_LONG
            ).show()
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e("socket", "Error connecting ${it[0]}")
        }
    }

    private fun toolbarInit() {
        this.setSupportActionBar(toolbar)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)
        this.supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}