package com.example.projectppsi.model.response.add_land

data class Location(
    val address: String,
    val city: String,
    val latitude: String,
    val longitude: String,
    val postal_code: String,
    val region: String
)