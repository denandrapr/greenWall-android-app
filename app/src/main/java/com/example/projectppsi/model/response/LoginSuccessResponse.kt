package com.example.projectppsi.model.response

data class LoginSuccessResponse(
    val status: String,
    val success: Boolean,
    val messages: String,
    val token: String
)