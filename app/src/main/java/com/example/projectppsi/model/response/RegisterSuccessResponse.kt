package com.example.projectppsi.model.response

data class RegisterSuccessResponse(
    val messages: String,
    val status: Int,
    val success: Boolean,
    val token: String
)