package com.example.projectppsi.model.body

data class InboxListTempBody (
    var inboxName: String,
    var inboxLastChat: String
)