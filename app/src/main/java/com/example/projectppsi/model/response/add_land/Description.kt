package com.example.projectppsi.model.response.add_land

data class Description(
    val area: String,
    val dueDate: String,
    val text: String
)