package com.example.projectppsi.model.response.add_land

data class CreatedBy(
    val __v: Int,
    val _id: String,
    val about: String,
    val avatar: Any,
    val createdAt: String,
    val email: String,
    val role: String,
    val updatedAt: String,
    val username: String
)