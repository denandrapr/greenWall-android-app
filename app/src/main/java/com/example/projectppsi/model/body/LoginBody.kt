package com.example.projectppsi.model.body

data class LoginBody(
    var email: String?,
    var password: String?
)