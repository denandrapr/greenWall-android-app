package com.example.projectppsi.model.response.article_limited_five

data class ArticleLimitedFiveResponse(
    val article: List<Article>,
    val messages: String,
    val status: Int,
    val success: Boolean
)