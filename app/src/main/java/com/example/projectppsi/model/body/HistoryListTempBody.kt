package com.example.projectppsi.model.body

data class HistoryListTempBody (
    var historyTitle: String,
    var historyLocation: String,
    var historyProgress: String
)