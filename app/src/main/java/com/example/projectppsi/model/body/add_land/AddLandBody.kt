package com.example.projectppsi.model.body.add_land

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddLandBody(
    val description: Description,
    val image: List<String>,
    val location: Location,
    val title: String
) : Parcelable