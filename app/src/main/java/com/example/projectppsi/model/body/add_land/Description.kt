package com.example.projectppsi.model.body.add_land

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Description(
    val area: String,
    val dueDate: String,
    val text: String
): Parcelable