package com.example.projectppsi.model.body

data class ArticleListTempBody(
    var title: String,
    var date: String,
    var user: String,
    var images: List<String>
)