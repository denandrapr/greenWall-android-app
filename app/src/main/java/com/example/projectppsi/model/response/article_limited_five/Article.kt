package com.example.projectppsi.model.response.article_limited_five

data class Article(
    val __v: Int,
    val _id: String,
    val author: String,
    val content: String,
    val createdAt: String,
    val image: List<String>,
    val status: String,
    val title: String,
    val updatedAt: String
)