package com.example.projectppsi.model.response.add_land

data class AddLandResponse(
    val messages: String,
    val `property`: Property,
    val status: Int,
    val success: Boolean
)