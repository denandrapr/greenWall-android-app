package com.example.projectppsi.model.body

data class LandListTempBody (
    var landTitle: String,
    var landLord: String,
    var landLocation: String,
    var daysRemaining: String
)