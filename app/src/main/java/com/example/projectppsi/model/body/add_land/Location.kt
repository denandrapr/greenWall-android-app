package com.example.projectppsi.model.body.add_land

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
    val address: String,
    val city: String,
    val latitude: String,
    val longitude: String,
    val postal_code: String,
    val province: String
): Parcelable