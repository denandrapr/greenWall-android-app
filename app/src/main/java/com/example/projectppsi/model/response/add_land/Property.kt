package com.example.projectppsi.model.response.add_land

data class Property(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val createdBy: CreatedBy,
    val description: Description,
    val image: List<String>,
    val location: Location,
    val participants: List<Any>,
    val status: String,
    val title: String,
    val updatedAt: String,
    val verifiedBy: Any
)