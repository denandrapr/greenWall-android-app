package com.example.projectppsi.networking

import android.content.Context
import com.example.projectppsi.util.AppConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkFactory {

    fun serviceNoAuth(): ApiServices {
        val client = OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.HEADERS
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build()

        fun retrofit() : Retrofit = Retrofit.Builder()
            .baseUrl(AppConfig.BASE_API_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        return retrofit().create(ApiServices::class.java)
    }

    fun serviceWithAuth(context: Context): ApiServices {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        logging.level = HttpLoggingInterceptor.Level.HEADERS

        val client = OkHttpClient.Builder()
        client.addInterceptor{chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .header("Authorization", "${AppConfig.getToken(context)}")
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        client.addNetworkInterceptor(logging).build()
        val okClient = client.build()

        fun retrofit() : Retrofit = Retrofit.Builder()
            .baseUrl(AppConfig.BASE_API_URL)
            .client(okClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        return retrofit().create(ApiServices::class.java)
    }

}