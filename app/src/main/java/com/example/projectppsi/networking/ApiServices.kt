package com.example.projectppsi.networking

import com.example.projectppsi.model.body.add_land.AddLandBody
import com.example.projectppsi.model.response.LoginSuccessResponse
import com.example.projectppsi.model.response.RegisterSuccessResponse
import com.example.projectppsi.model.response.add_land.AddLandResponse
import com.example.projectppsi.model.response.article_limited_five.ArticleLimitedFiveResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiServices {

    @FormUrlEncoded
    @POST("/api/v1/auth/signin")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ) : Response<LoginSuccessResponse>

    @FormUrlEncoded
    @POST("/api/v1/auth/signup")
    suspend fun userRegister(
        @Field("username") username: String,
        @Field("email") email: String,
        @Field("password") password: String
    ) : Response<RegisterSuccessResponse>

    @FormUrlEncoded
    @POST("/api/v1/properties")
    suspend fun addLand(
        @Body body: AddLandBody
    ) : Response<AddLandResponse>

    @GET("/api/v1/articles")
    suspend fun getArticleLimitedFive() : Response<ArticleLimitedFiveResponse>
}