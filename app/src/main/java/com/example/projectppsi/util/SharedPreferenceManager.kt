package com.example.projectppsi.util

import android.content.Context

class SharedPreferenceManager private constructor(
    private val context: Context
){
    val checkLogin: Boolean
        get() {
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("token", "-1") != "-1"
        }

    val getToken: String?
        get() {
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("token", "null")
        }

    fun saveToken(token: String?){
        val sharedPreference = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        editor.putString("token", token)

        editor.apply()
    }

    fun clearAllSharedPref() {
        val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private const val SHARED_PREF_NAME = "greenWallPref"
        private var mInstance: SharedPreferenceManager? = null
        @Synchronized
        fun getInstance(mContext: Context): SharedPreferenceManager {
            if (mInstance == null) {
                mInstance = SharedPreferenceManager(mContext)
            }
            return mInstance as SharedPreferenceManager
        }
    }
}