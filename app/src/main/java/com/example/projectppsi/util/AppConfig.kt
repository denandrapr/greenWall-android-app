package com.example.projectppsi.util

import android.content.Context

object AppConfig {
    const val BASE_API_URL = "http://178.128.222.175:3001"
    const val CHAT_SERVER_URL = "http://178.128.222.175:5001"

    fun getToken(context: Context): String? {
        return SharedPreferenceManager.getInstance(context).getToken
    }
}