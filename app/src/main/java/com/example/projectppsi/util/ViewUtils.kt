package com.example.projectppsi.util

import android.content.Context
import android.util.Log
import android.widget.Toast

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun log(message: String) {
    Log.d("Response ", message)
}